# Assignment 2 - Token-based DME algorithm for ring topology 

## Input:
    List of requests with parameters that should be issued during the simulation.
    Provided example:
    
requests = [
			Request(1, 2, network[0]), 
			Request(2, 2, network[3]),
			]

Parameters in each constructor have the following meaning:
- the first one: timestamp
- the second one: time that will be spent in CS
- the last one: link to the candidate by his id


    
## Output: 
Nodes' states over the time.

![](output.png)

## Usage: 
run app as usual python program without any additional arguments.

