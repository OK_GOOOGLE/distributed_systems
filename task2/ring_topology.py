from collections import deque
from random import randint


N = 5 # number of sites
R = 3 # number of requests
duration = 2

class State():
	idle = 0
	in_cs = 1
	waiting = 2


class Request:
	def __init__(self, timestamp, duration, candidate):
		self.timestamp = timestamp
		self.candidate = candidate
		self.duration = duration
		self.status = 0 # 0 - not resolved, 1 - resolved

	def create_Request(self, timestamp):
		print(f"Creating req for node: {self.candidate.id} with timestamp: {timestamp}")
		if self.timestamp == timestamp:
			self.candidate.enter_CS()

	def isResolved(self):
		return self.status

	def initTime(self):
		return self.timestamp

	def reqDuration(self):
		return self.duration

	def resolve(self):
		self.status = 1

	def time(self):
		return self.timestamp

class Token:
    def __init__(self):
        self.queue = []

class Node:
	def __init__(self, number):
		self.time = 0
		self.id = number
		self.next = None
		self.state = State.idle
		self.remain_time_in_CS = -1
		self.pending_queue = [] 
		self.request = None
		self.token = None

	def send_token(self):
		if self.next.token != None:
			raise Exception(f"Overriding token of node {self.next.id} by former.")
		self.next.token = self.token
		self.token = None

	def adds_to_token_queue_list(self, asking_node):
		self.pending_queue.extend(asking_node.pending_queue)


	def permits(self, asking_node):
		print(f"Node {self.id} sends goahead to {asking_node.id}")
		asking_node.recieved_goaheads[self.id] = 1
		# remove asking node from pending list
		self.pending_go_aheads[asking_node.id] = 0

	def process(self):
		if self.state == State.in_cs:
			self.remain_time_in_CS -= 1
			if self.remain_time_in_CS == 0:
				self.leave_CS()

		if self.token is None:
			self.next.pending_queue.extend(self.pending_queue)
			self.pending_queue.clear()
		else:
			self.process_with_token()
			
	def process_with_token(self):
		self.token.queue.extend(self.pending_queue)
		self.pending_queue.clear()
		if self.isInCS():
			pass
		elif self.isWaiting():
			if self.token.queue[0].id == self.id:
				self.occupy_CS()

		if not self.isInCS():
			self.send_token()

	#CS = Critical Section
	def leave_CS(self):
		print(f"Node {self.id} is leaving CS")
		if not self.isInCS():
			msg = "Can not leave CS. Node " + str(self.id) + " is not in CS. state: " + str(self.state)
			raise Exception(msg)
		self.remain_time_in_CS = -1
		self.state = State.idle

	def enter_CS(self):
		self.state = State.waiting
		self.pending_queue.append(self)

	def occupy_CS(self):
		self.token.queue = self.token.queue[1:]
		print(f"Node {self.id} is entering CS")
		self.state = State.in_cs
		self.remain_time_in_CS = duration

	def needs_to_leave(self):
		if self.remain_time_in_CS == 0:
			return True

	def isInCS(self):
		return self.state == State.in_cs

	def isWaiting(self):
		return self.state == State.waiting

	def isIdle(self):
		return self.state == State.idle


#creating sample network with ring topology
network = [Node(0)]
for i in range(1, N):
	network.append(Node(i))
	network[i-1].next = network[i]
network[-1].next = network[0]

for node in network:
	print(node.id, ' next: ', node.next.id)


#Request(timestamp, duration, node)
requests = [
			Request(1, duration, network[0]), 
			Request(2, duration, network[3]), 
			Request(6, 1, network[2])
		   ]

network[1].token = Token()

# main loop
globalTime = 0 # initial time
while globalTime < 15:
	for request in requests:
		if request.initTime() == globalTime:
			# creating req and trying respective node to enter CS
			request.create_Request(globalTime)

	for node in network:
		node.time += 1
		node.process()
		node.remain_time_in_CS -= 1 if node.remain_time_in_CS > -1 else 0
		print(f"Node: {node.id}, time: {node.time} state: {node.state}, pending nodes: {node.pending_queue}, Tremain: {node.remain_time_in_CS}")		
	print()
	globalTime += 1


