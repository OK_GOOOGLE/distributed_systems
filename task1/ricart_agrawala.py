from collections import deque
from random import randint

N = 5 # number of sites
R = 3 # number of requests

# list of possible states
class State():
	idle = 0
	in_cs = 1
	waiting = 2

class Request:
	def __init__(self, timestamp, duration, candidate):
		self.timestamp = timestamp
		self.candidate = candidate
		self.duration = duration
		self.status = 0 # 0 - not resolved, 1 - resolved

	def create_Request(self, timestamp, request):
		print("Creating req for node: ", self.candidate.id, " with timestamp:", timestamp)
		self.candidate.request = self
		self.candidate.try_enterCS()

	def isResolved(self):
		return self.status

	def initTime(self):
		return self.timestamp

	def reqDuration(self):
		return self.duration

	def resolve(self):
		self.status = 1

	def time(self):
		return self.timestamp


class Node:
	def __init__(self, number):
		self.time = 0 
		self.id = number
		self.state = State.idle
		self.remainTimeInCS = -1
		self.recieved_goaheads = [0 for n in range(N)] # waiting for approval from other nodes. 
		self.pending_go_aheads  = [0 for n in range(N)]
		self.request = None

	def can_enterCS(self):
		for i, value in enumerate(self.recieved_goaheads):
			if i == self.id:
				continue
			if value == 0:
				return False
		return True

	def try_enterCS(self):
		if self.isInCS():
			raise Exception(f"Can not enter CS. Node {self.id} is currently in CS")
		self.state = State.waiting
		for node in network:
			if node is not self:
				self.asks_for_permission(node)

		if self.can_enterCS():
			self.enterCS(self.request.reqDuration())
		
		
		# self.time += 1
	def decides_to_permit(self, asking_node):
		if self.request.timestamp > asking_node.request.timestamp:
			return True
		elif self.request.timestamp == asking_node.request.timestamp:
			return True if self.id > asking_node.id else False
		return False

	def asks_for_permission(self, node_to_ask):
		if node_to_ask.isIdle():
			node_to_ask.permits(self)
		elif node_to_ask.isWaiting():
			if node_to_ask.decides_to_permit():
				node_to_ask.permits(self)
			else:
				node_to_ask.adds_to_its_pending_list(self)
		elif node_to_ask.isInCS():
			node_to_ask.adds_to_its_pending_list(self)

	def adds_to_its_pending_list(self, asking_node):
		self.pending_go_aheads[asking_node.id] = 1



	def permits(self, asking_node):
		print(f"Node {self.id} sends goahead to {asking_node.id}")
		asking_node.recieved_goaheads[self.id] = 1
		# remove asking node from pending list
		self.pending_go_aheads[asking_node.id] = 0



	#CS = Critical Section
	def leaveCS(self):
		print(f"Node {self.id} is leaving CS")
		if not self.isInCS():
			msg = "Can not leave CS. Node " + str(self.id) + " is not in CS. state: " + str(self.state)
			raise Exception(msg)

		self.remainTimeInCS = 0
		self.state = State.idle
		for pending_id in range(len(self.pending_go_aheads)):
			if self.pending_go_aheads[pending_id]:
				self.permits(network[pending_id])
				network[pending_id].try_enterCS()


	def enterCS(self, timeInCS):
		print(f"Node {self.id} enters CS")
		self.remainTimeInCS = timeInCS
		self.state = State.in_cs
		self.request.resolve()

	def needs_to_leave(self):
		if self.remainTimeInCS == 0:
			return True

	def isInCS(self):
		return self.state == State.in_cs

	def isWaiting(self):
		return self.state == State.waiting

	def isIdle(self):
		return self.state == State.idle


#creating sample network with ring topology
network = [Node(0)]
for i in range(1, N):
	network.append(Node(i))

for node in network:
	print(f"{node.id}: {node.recieved_goaheads}, state: {node.state}")
print()

# note: we assume that every node can ask for CS only once
# Request(timestamp, duration, node)
requests = [
			Request(1, 3, network[0]), 
			Request(2, 3, network[3]),
			Request(6, 1, network[2])
			]

# main loop
globalTime = 0 # initial time
while globalTime < 5:
	for request in requests:
		if request.initTime() == globalTime:
			# creating req and trying respective node to enter CS
			request.create_Request(globalTime, request)

	for node in network:
		node.time += 1
		if node.needs_to_leave():
			node.leaveCS()
		node.remainTimeInCS -= 1 if node.remainTimeInCS > -1 else 0
		print(f"Node: {node.id}, time: {node.time} state: {node.state}, recieved goaheads: {node.recieved_goaheads} pending nodes: {node.pending_go_aheads}, Tremain: {node.remainTimeInCS}")
	print()
	globalTime += 1


k = 0
for i in range(len(requests)):
	if requests[i].status == 0:
		k += 1
		print(f"Request: {i} is not resolved")
if k == 0:
	print("All requests have been resolved")





