# Assignment 4 - Centralized Deadlock Detection Algorithm

## Input:
    Resource status tables for each node at given moment of time. Tables are provided in code.
    Provided example:
    
    {
			 'p1': [1, 0, -1], 
			 'p2': [0, 0, 1], 
			 'p3': [-1, 0, -1], 
			 'p4': [0, 0, 0]
			 },
			 {
			 'p1': [0, -1, 0], 
			 'p2': [0, 0, 0], 
			 'p3': [0, 0, 0], 
			 'p4': [-1, 1, -1]
    }
where -1 stands for process requesting the resource. 1 stands for currently occupying the resource. 0 stands for neutrality.
    
## Output: 
Presence or abscence of deadlock.

![](deadlock.png)

## Usage: 
run app as usual python program without any additional arguments.

