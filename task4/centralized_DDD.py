def create_general_table(nodes):
	result = {}
	for process, resources in nodes[0].items():
		result[process] = [res for res in resources]

	for node in nodes:
		for process, resources in node.items():
			for i in range(len(resources)):
				if result[process][i] == 0:
					result[process][i] = resources[i]
	return result

def create_graph(general_table):
	graph = {}
	for process in general_table: 
		graph[process] = []

	resources = len(general_table['p1'])

	for i in range(resources):
		holder = None
		candidates = []
		for process in general_table:
			if general_table[process][i] == 1 and not holder:
				holder = process 

			if general_table[process][i] == -1:
				candidates.append(process)

		for candidate in candidates:
			graph[candidate].append(holder)

	return graph

def traverseFromVertex(process, visited, stack, graph):
	visited[process] = True
	stack[process] = True
	for neighbour in graph[process]:
		if visited[neighbour] == False:
			return traverseFromVertex(neighbour, visited, stack, graph)
		else: 
			return stack[neighbour]
  
    # popping out last node from stack 
	stack[process] = False
	return False

def find_deadlock(graph):
	size = len(graph.keys())
	node = 0
	visited = {p: False for p in graph}
	stack = {p: False for p in graph}
	for process in graph:
		if visited[process] == False:
			result = traverseFromVertex(process, visited, stack, graph)
			print("Processes in deadlock: ", end='')
			for i in stack:
				if stack[i]:
					print(i, end=' ')
			print()
			return result
	node += 1
	return False


nodes = [
			 {
			 'p1': [1, 0, -1], 
			 'p2': [0, 0, 1], 
			 'p3': [-1, 0, -1], 
			 'p4': [0, 0, 0]
			 },
			 {
			 'p1': [0, -1, 0], 
			 'p2': [0, 0, 0], 
			 'p3': [0, 0, 0], 
			 'p4': [-1, 1, -1]
			 }
	    ]




general_table = create_general_table(nodes)
print("General Table(based on each node's table):")
for key, value in general_table.items():
	print(f"{key}: {value}")

print()

graph = create_graph(general_table)
print("Wait-for graph:")
for key, value in graph.items():
	print(f"{key}: {value}")

print()

if find_deadlock(graph):
	print("There is a deadlock")
else:
	print("No deadlock found")


