# Assignment 5 - Mitchell-Merritt Deadlock Detection Algorithm

## Input:
Wait-for graph:

wait_for_graph = {
				  0: [1, 3], 
				  1: [], 
				  2: [0, 1], 
				  3: [0, 1]
			  	 }
    
## Output: 
Presence or abscence of deadlock.

![](deadlock.png)

## Usage: 
run app as usual python program without any additional arguments.

