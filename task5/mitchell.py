from random import randint
from random import sample

class Node:
	def __init__(self, num):
		self.id = num
		self.pub_key = None
		self.priv_key = None
		self.blocking_nodes = [] # nodes by which self is blocked
		self.blocked_nodes = [] # nodes blocked by self

# backpropagate pub key to nodes blocked by self
	def back_propagate_pubkey(self, network):
		for blocked_node in self.blocked_nodes:
			print(f"{self.id} propagating to {network[blocked_node].id}, sender:{self.pub_key},{self.priv_key} recvr:{network[blocked_node].pub_key},{network[blocked_node].priv_key}")
			if network[blocked_node].priv_key == self.pub_key:
				return True
			else: 
				network[blocked_node].pub_key = max(self.pub_key, network[blocked_node].pub_key)
				network[blocked_node].back_propagate_pubkey(network)


def init_keys(network):
	size = len(network)
	keys = sample(range(0, size), size)
	for node in network:
		node.pub_key = keys[node.id]
		node.priv_key = keys[node.id]

def update_keys(network):
	print("after updaate")
	for node in network:
		for blocking_node in node.blocking_nodes:
			node.priv_key = max(node.priv_key, network[blocking_node].priv_key) + 1
			node.pub_key = node.priv_key

	for node in network:
		print(f"{node.id}: pub: {node.pub_key}, priv: {node.priv_key}")

def add_blocked_blocking(network, graph):
	for node in network:
		node.blocking_nodes = graph[node.id][:]

	#find blocked nodes
	for key, blocking_nodes in graph.items():
			for node in blocking_nodes:
				network[node].blocked_nodes.append(key)

def find_deadlock(graph):
	size = len(graph.keys())
	network = [Node(id) for id in range(size)]
	init_keys(network)
	print("before updaate")
	for node in network:
		print(f"{node.id}: pub: {node.pub_key}, priv: {node.priv_key}")

	add_blocked_blocking(network, graph)
	update_keys(network)
	t = 0
	while t < 100:
		for node in network:
			if (node.back_propagate_pubkey(network)):
				return True
		t += 1
	return False


wait_for_graph = {
			  0: [1, 3], 
			  1: [], 
			  2: [0, 1], 
			  3: [0, 1]
			  }

print("Wait-for graph:")
for key, value in wait_for_graph.items():
	print(f"{key}: {value}")

print()

if find_deadlock(wait_for_graph):
	print("There is a deadlock")
else:
	print("No deadlock found")