# Distributed Systems

# Task 0

for the graph:  
	``1 : [3, 4, 5]``  
 	``2 : [4]``  
	``3 : [2, 4]``  
	``4 : [2]``  
	``5 : [1, 2]``  
the output is:  
``vertex  1 [1, 3, 2, 4, 5]``  
``vertex  2 : can not be the starting one``  
``vertex  3 : can not be the starting one``  
``vertex  4 : can not be the starting one``  
``vertex  5 [5, 1, 3, 2, 4]``