import json

# DFS for oriented graph

class Graph(object):	

	def __init__(self):
		self.visited = []
		self.visitOrder = []
		self.adjList = {}

	def exportGraph(self):
		with open("graph.txt", "w") as file:
			json.dump(self.adjList, file, indent=2)

	def importGraph(self):
		with open("graph.txt", "r") as file:
			self.adjList = json.load(file)

		
	def printGraph(self):
		for i in self.adjList:
			print(i, ":", self.adjList[i])

	def traverseFromVertex(self, vertex):
		if vertex not in self.visited:
			self.visitOrder.append(vertex)
			self.visited.append(vertex)
			for i in self.adjList[vertex]:
				self.traverseFromVertex(i)

	def traverseGraph(self):
		for i in self.adjList:
			self.visited = []
			self.visitOrder = []
			g.traverseFromVertex(i)
			if len(self.visitOrder) < 5:
				print("vertex ", i, ": can not be the starting one")
			else:
				print("vertex ", i, self.visitOrder)

		


g = Graph()
g.printGraph()
print("=== output ===")
g.traverseGraph()
g.importGraph()


