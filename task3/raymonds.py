num_requests = 0

class Node:
	def __init__(self, id):
		self.id = id
		self.parent = None
		self.token = False
		self.queue = []

	def if_self_turn(self):
		global num_requests
		print(f"Node {self.id} : is entering CS")
		num_requests -= 1
		self.queue.pop(0)
		print(f"Node {self.id} : is leaving CS")
		if len(self.queue) > 0:
			child = self.queue.pop(0)
			self.parent = child
			child.parent = None
			if len(self.queue) > 0:
				self.parent.queue.append(self)

	def if_not_self_turn(self):
		child = self.queue.pop(0)
		self.parent = child
		child.parent = None
		if len(self.queue) > 0:
			self.parent.queue.append(self)

	def process_no_parent(self):
		if len(self.queue) > 0:
					if self.queue[0] == self:
						self.if_self_turn()
					else:
						self.if_not_self_turn()	

	def process(self):
		if self.parent is None:
			self.process_no_parent()

	def create_request(self, *network):
		if len(network) == 0:
			self.if_empty()
		else:
			if network[0] not in self.queue:
				self.queue.append(network[0])
		if self.parent:
			self.parent.create_request(self)

	def if_empty(self):
		global num_requests
		num_requests += 1
		if self not in self.queue:
			self.queue.append(self)


def build_tree(tree):
	for node, children_network in tree.items():
		for children_node in children_network.keys():
			children_node.parent = node

		build_tree(children_network)


def select(tree, id):
	for node, children_network in tree.items():
		if node.id == id:
			return node
		else:
			result = select(children_network, id)
			if result is not None:
				return result
	return None


def print_tree_status():
	ite = [select(network, i) for i in range(tree_size)]
	for node in ite:
		print(f"Time: {time}, Node id: {node.id}, Parent: {node.parent.id if node.parent else 'None'} , pending: {', '.join(str(n.id) for n in node.queue)}")

tree_size = 7
network = {
		Node(0): {
				Node(3): {
						Node(4): {},
						},
				Node(1): {
						Node(2): {},
						Node(5): {
							Node(6): {},
						},
						}
				}
		}

build_tree(network)
select(network, 3).create_request()
select(network, 6).create_request()
select(network, 0).create_request()
time = 0
print_tree_status()

while num_requests != 0 and time <= 1000:
	for node in [select(network, i) for i in range(tree_size)]:
		node.process()
	if time % 2 == 0:
		print_tree_status()
		print()
	time += 1
print_tree_status()
