# Assignment 3 - Raynolds Algorithm

## Input:
    List of nodes with tree topology.
    Provided example:
network_tree = {
				Node(0): {
						Node(3): {
								Node(4): {},
								},
						Node(1): {
								Node(2): {},
								Node(5): {
									Node(6): {},
								},
								}
						}
				}

     Requests:
    select_node(network, 3).create_request()
	select_node(network, 6).create_request()
	select_node(network, 0).create_request() 

    
## Output: 
Nodes' states over the time.

![](output.png)

## Usage: 
run app as usual python program without any additional arguments.

